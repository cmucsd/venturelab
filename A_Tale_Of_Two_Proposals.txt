Worst case scenario:

Key partnership

All customer contacts are managed, mediated, and regulated on a need-to-know basis by the main contractor, a large defense company.


Key activities

Development and research of sub-blocks and modules of statistical signal processing, on a need-to-know basis.


Key resources

1. Access to all techncal information and data about the complete signal communication/detection/interpretation chain
2. Access to industrial grade IC design tools
3. Access to IC foundry technology
4. Access to test equipment

Access to 1. (and in part 3.) is restricted to a need-to-know basis by the main contractor


Value propositions

Using cutting-edge research results in statistical signal processing, 

enable monolithic, high performance, ultra high energy efficiency, signal recognition integrated circuits for communication, signal detection, and analysis


Customer relationships

No immediate access to customer.

Results are communicated and reported to the main contractor, evaluated by the main contractor and forwarded or withheld at the main contractor's discretion to further their business interest. 


Channels

Weekly progress reports of research


Customer segments

End customer:
Military, 
defense industry

One could count 
Abdulrahman al-Awlaki et al. as target customer, too.


Cost structure

Sustenance of researchers (rent, food, NON-SOCIALIZED HEALTH INSURANCE), 
internet access, 
computers, 
access to scientific journals,
IC DESIGN TOOLS, 
ACCESS TO IC FOUNDRY SERVICE


Revenue streams

DoD contracts are awarded to the main contractor, who partially redistributes grant money to subcontractors under the constraint of maximizing the main contractor's share of the grant money.




Same technial field, but better:
===============================

Key partnership

Emergent do-it-yourself hardware infrastructure
Non-profits
Academia without ties to military reserach


Key activities

Development and research of low-power, portable radio transceivers utilizing statistical signal processing methods.


Key resources

1. Access to all techncal information and data about the complete signal communication/detection/interpretation chain
2. Access to electronics design tools
3. Access to mechanical design tools
4. Access to test equipment
5. Access to electronics manufacturing (PCB mfg.)
6. Access to mechanical manufacturing (3D printing)


Value propositions

Using cutting-edge research results in statistical signal processing, 

provide high performance, ultra high energy efficiency robust, portable transceivers for peer-to-peer communication 
secure against interception and/or censorship by corporate or government authority.

Customer relationships

Direct access to customer through internet fora.


Channels

Results are communicated and reported to the customer through a website similar to http://mightyohm.com/blog/products/
through blogs, and publications.
Possible distribution through sites like http://adafruit.com/

Customer segments

Technically savvy private individuals "tinkerers"
People in areas with underdeveloped communication infrastructure
People in areas with communication infrastructure restricted, monitored, or censored by corporate or government entities.


Cost structure

Sustenance of researchers (rent, food, NON-SOCIALIZED HEALTH INSURANCE), 
internet access, 
computers, 
access to scientific journals,
electronic design tools, 
manufacturing cost,
web site maintenance,
travel expenses,
legal expenses to protect business against FCC (or similar organizations) intervention


Revenue streams

Crowdsourcing (kickstarter etc.)
Sale of kits and finished units

